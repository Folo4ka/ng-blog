import {Environment} from './interface';

export const environment: Environment = {
  production: true,
  apiKey: 'AIzaSyADEJE0oXckLRtGS2ZJ1hTfVNlwsb_wdLI',
  fbDbUrl: 'https://angular-blog-11fab.firebaseio.com/',
  siteTitle: 'NgBlog'
};
