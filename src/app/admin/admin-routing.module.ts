import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminLayoutComponent} from './shared/components/admin-layout/admin-layout.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {DashboardPageComponent} from './dashboard-page/dashboard-page.component';
import {AuthGuard} from './shared/services/auth.guard';
import {CreatePageComponent} from './create-page/create-page.component';
import {EditPageComponent} from './edit-page/edit-page.component';
import {LoginGuard} from './shared/services/login.guard';
import {environment} from '../../environments/environment';

const routes: Routes = [
  {
    path: '', component: AdminLayoutComponent, children: [
      {path: '', redirectTo: '/admin/login', pathMatch: 'full'},
      {path: 'login', component: LoginPageComponent, canActivate: [LoginGuard], data: {title: `${environment.siteTitle} | Вход`}},
      {path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthGuard], data: {title: `${environment.siteTitle} | Дашбоард`}},
      {path: 'create', component: CreatePageComponent, canActivate: [AuthGuard], data: {title: `${environment.siteTitle} | Создать пост`}},
      {path: 'post/:id/edit', component: EditPageComponent, canActivate: [AuthGuard], data: {title: `${environment.siteTitle} | Изменить пост`}}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
